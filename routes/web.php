<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::group(['namespace' => 'Backend', 'middleware' => 'auth:web'], function () {
//    Route::get('/', function (
//    ) {
//        return view('welcome');
//    });

Route::get('/', 'Frontend\HomeController@index')->name('index');
//Route::get('/logout', 'UserController@logout')->name('logout');
//});

//Author Dashboard Route
Route::get('/authorDashboard', 'Frontend\AuthorController@authorDashboard')->name('authorDashboard');
Route::get('/addPost', 'Frontend\AuthorController@addPost')->name('addPost');
Route::post('/addPostProcess', 'Frontend\AuthorController@addPostProcess')->name('addPostProcess');
Route::get('/viewPost', 'Frontend\AuthorController@viewPost')->name('viewPost');
Route::get('/editPost/{id}', 'Frontend\AuthorController@editPost')->name('editPost');
Route::put('/editPostProcess/{id}', 'Frontend\AuthorController@editPostProcess')->name('editPostProcess');
Route::get('/deletePost', 'Frontend\AuthorController@deletePost')->name('deletePost');

Route::group(['prefix' => 'admin'], function () {
//    Route::get('/', function (
//    ) {
//        return view('welcome');
//    });
    Route::get('/', 'Backend\UserController@adminIndex')->name('adminIndex');
    Route::get('/category', 'Backend\CategoryController@category')->name('category');
    Route::get('/createCategory', 'Backend\CategoryController@createCategory')->name('createCategory');
    Route::post('/createCategoryProcess', 'Backend\CategoryController@createCategoryProcess')->name('createCategoryProcess');
    Route::get('/editCategory/{id}', 'Backend\CategoryController@editCategory')->name('editCategory');
    Route::put('/editCategoryProcess/{id}', 'Backend\CategoryController@editCategoryProcess')->name('editCategoryProcess');
    Route::get('/deleteCategory/{id}', 'Backend\CategoryController@deleteCategory')->name('deleteCategory');
    Route::get('/articleList', 'Backend\ArticleController@articleList')->name('articleList');
    Route::get('/activeArticle/{id}', 'Backend\ArticleController@activeArticle')->name('activeArticle');
    Route::get('/InactiveArticle/{id}', 'Backend\ArticleController@InactiveArticle')->name('InactiveArticle');

//Route::get('/logout', 'UserController@logout')->name('logout');


    // User Routes
    Route::get('/registration', 'Frontend\UserController@registration')->name('registration');
    Route::post('/registrationProcess', 'Frontend\UserController@registrationProcess')->name('registrationProcess');


    // Admin Routes

    Route::get('/authorList', 'Backend\UserController@authorList')->name('authorList');
    Route::get('/activeAuthor/{id}', 'Backend\UserController@activeAuthor')->name('activeAuthor');
    Route::get('/InactiveAuthor/{id}', 'Backend\UserController@InactiveAuthor')->name('InactiveAuthor');

//    Route::



});
