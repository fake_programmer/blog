 <!--Sidebar Start-->
 <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <aside class="sidebar default-sidebar right-sidebar">
    
        <!--Social Widget-->
        <div class="sidebar-widget sidebar-social-widget">
            <div class="sidebar-title">
                <h2>Follow Us</h2>
            </div>
            <ul class="social-icon-one alternate">
                <li><a href="#"><span class="fa fa-facebook"></span></a></li>        
                <li class="twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>
                <li class="g_plus"><a href="%"><span class="fa fa-google-plus"></span></a></li>
            </ul>
        </div>
        <!--End Social Widget-->
        
        <!--Category Widget-->
        <div class="sidebar-widget categories-widget">
            <div class="sidebar-title">
                <h2>Categories</h2>
            </div>
            <ul class="cat-list">
                <li class="clearfix"><a href="category.html">PHP</a></li>
                <li class="clearfix"><a href="category.html">JS</a></li>
                <li class="clearfix"><a href="category.html">HTML</a></li>
            </ul>
        </div>
        <!--End Category Widget-->
        
        <!--Category Widget-->
        <div class="sidebar-widget categories-widget">
            <div class="sidebar-title">
                <h2>Recent Post</h2>
            </div>
            <article class="widget-post">
                <figure class="post-thumb"><a href="singlepage.html"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="assets/images/resource/news-1.jpg" alt=""></a><div class="overlay"><span class="icon qb-play-arrow"></span></div></figure>
                <div class="text"><a href="singlepage.html">Article Title</a></div>
                <div class="post-info">22-06-2019</div>
            </article>
        </div>
        
    </aside>
</div>
<!--Sidebar End-->