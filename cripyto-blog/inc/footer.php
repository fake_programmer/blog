<!--Main Footer-->
<footer class="main-footer">
    
    <div class="widgets-section">
        <div class="auto-container">
            <div class="row clearfix">
                
                <!--Widget Column-->
                <div class="widget-column col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-widget tweets-widget">
                        <h2>Footer Menu</h2>
                        <div class="footer-menu">
                            <ul>
                                <li><a href="index.html">Home</a></li>
                                <li><a href="about.html">About Us</a></li>
                                <li><a href="contact.html">Contact Us</a></li>
                                <li><a href="login.html">My Account</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
                <!--Widget Column / Instagram Widget-->
                <div class="widget-column col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-widget isntagram-widget">
                        <h2>About Us</h2>
                        <div class="footer-menu">
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui, dolore.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui, dolore.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui, dolore.</p>
                        </div>
                    </div>                        
                </div>                    
                <!--Widget Column / Newsletter Widget-->
                <div class="widget-column col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-widget newsletter-widget">
                        <h2>Newsletter</h2>
                        <div class="newsletter-form">
                            <form method="post" action="">
                                <div class="form-group">
                                    <input type="text" name="name" value="" placeholder="Name" required="">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" value="" placeholder="Email" required="">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="theme-btn btn-style-one">Subscribe</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>                    
            </div>
        </div>
    </div>        
    <!--Footer Bottom-->
    <div class="footer-bottom">
        <!--Copyright Section-->
        <div class="copyright-section">
            <div class="auto-container">
                <div class="row clearfix">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center">&copy; Copyright cryptofinancials. All rights reserved. <span class="dev">Developed By QSS</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--End Main Footer-->    
</div>
<!--End pagewrapper-->
<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-double-up"></span></div>

<script src="assets/js/jquery.js"></script> 
<!--Revolution Slider-->
<script src="assets/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="assets/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="assets/plugins/revolution/js/extensions/revolution.extension.video.min.js"></script>
<script src="assets/js/main-slider-script.js"></script>
<!--End Revolution Slider-->
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/owl.js"></script>
<script src="assets/js/appear.js"></script>
<script src="assets/js/wow.js"></script>
<script src="assets/js/jquery.fancybox.js"></script>
<script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="assets/js/script.js"></script>
<script src="assets/js/color-settings.js"></script>
</body>
</html>
