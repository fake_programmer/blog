<!--Main Header Start-->
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Mycryptovision</title>
<!-- Stylesheets -->
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/plugins/revolution/css/settings.css" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="assets/plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="assets/plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<!--Color Switcher Mockup-->
<link href="assets/css/color-switcher-design.css" rel="stylesheet">

<!--Color Themes-->
<link id="theme-color-file" href="assets/css/color-themes/default-theme.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
<link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5b9f819ba51ed30011fe7083&product=inline-share-buttons"></script>
</head>

<body>

<div class="page-wrapper">
	
<!-- Preloader -->
<!-- <div class="preloader"></div> -->
	
<!-- Main Header -->
<header class="main-header">
	<!--Header-Upper-->
    <div class="header-upper">
    	<div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="logo-outer">
                        <div class="logo"><a href="index.html"><img src="assets/images/logo.png" alt="" /></a></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="header-search">
                        <form method="get" action="#">
                            <div class="form-group">
                                <input type="search" name="search" value="" placeholder="Find more articles, type your search..." required>
                                <button type="submit" class="search-btn">Search</button>
                            </div>
                        </form>
                    </div>
                </div>    
            </div>
		</div>
    </div>
    <!--End Header Upper-->
    <!--Header Lower-->
    <div class="header-lower">
    	<div class="auto-container">
            <div class="nav-outer clearfix">
                <!-- Main Menu -->
                <nav class="main-menu">
                    <div class="navbar-header">
                        <!-- Toggle Button -->    	
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div class="navbar-collapse collapse clearfix" id="bs-example-navbar-collapse-1">
                        <ul class="navigation clearfix">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Main Menu End-->
                <div class="outer-box">
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix" id="bs-example-navbar-collapse-1">
                            <ul class="navigation clearfix">
                                <li class="dropdown"><a href="">Sumon ArM<i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <li><a href="profile.html">Profile</a></li>
                                    <li><a href="#">Logout</a></li>
                                </ul>
                                
                                </li>
                                <li><a href="dashboard.html">Dashboard</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
               
                <!-- Hidden Nav Toggler -->
                 <div class="nav-toggler">
                     <button class="hidden-bar-opener"><span class="icon qb-menu1"></span></button>
                 </div>
                
            </div>
        	<div class="nav-outer clearfix">
                <!-- Main Menu -->
                <nav class="main-menu">
                    <div class="navbar-header">
                        <!-- Toggle Button -->    	
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div class="navbar-collapse collapse clearfix" id="bs-example-navbar-collapse-1">
                        <ul class="navigation clearfix">
                            <li class="current"><a href="index.html">Home</a></li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- Main Menu End-->
                <div class="outer-box">
                    <nav class="main-menu">
                        <div class="navbar-collapse collapse clearfix" id="bs-example-navbar-collapse-1">
                            <ul class="navigation clearfix">
                                <li><a href="login.html">Login</a></li>
                                <li><a href="register.html">Register</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
               
                <!-- Hidden Nav Toggler -->
                 <div class="nav-toggler">
                     <button class="hidden-bar-opener"><span class="icon qb-menu1"></span></button>
                 </div>
                
            </div>
        </div>
    </div>
    <!--End Header Lower-->

	<!--Sticky Header-->
    <div class="sticky-header">
        <div class="auto-container clearfix">
            <!--Logo-->
            <div class="logo pull-left">
                <a href="index.html" title=""><img src="assets/images/logo-small.png" alt="" /></a>
            </div>
            
            <!--Right Col-->
            <div class="right-col pull-right">
                <!-- Main Menu -->
                <nav class="main-menu">
                    <div class="navbar-header">
                        <!-- Toggle Button -->    	
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                    <div class="navbar-collapse collapse clearfix">
                        <ul class="navigation clearfix">
                            <li class="current"><a href="index.html">Home</a></li>
                            <li><a href="about.html">About</a></li>
                            <li><a href="contact.html">Contacts</a></li>
                        </ul>
                    </div>
                </nav><!-- Main Menu End-->
            </div>
            
        </div>
    </div>
    <!--End Sticky Header-->

</header>
<!--End Header Style Two -->

<!-- Hidden Navigation Bar -->
<section class="hidden-bar left-align">        
    <div class="hidden-bar-closer">
        <button><span class="qb-close-button"></span></button>
    </div>        
    <!-- Hidden Bar Wrapper -->
    <div class="hidden-bar-wrapper">
        <div class="logo">
        	<a href="index.html"><img src="assets/images/mobile-logo.png" alt="" /></a>
        </div>
        <!-- .Side-menu -->
        <div class="side-menu">
        	<!--navigation-->
            <ul class="navigation clearfix">
                <li class="current"><a href="index.html">Home</a></li>
                <li><a href="about.html">About</a></li>
                <li><a href="contact.html">Contacts</a></li>
                <li><a href="login.html">Login</a></li>
                <li><a href="register.html">Register</a></li>
            </ul>
        </div>
        <!-- /.Side-menu -->            
        <!--Options Box-->
        <div class="options-box">
        	<!--Sidebar Search-->
            <div class="sidebar-search">
                <form method="get" action="#">
                    <div class="form-group">
                        <input type="search" name="search" value="" placeholder="Search ..." required="">
                        <button type="submit" class="theme-btn"><span class="fa fa-search"></span></button>
                    </div>
                </form>
            </div>                
            <!--Social Links-->
            <ul class="social-links clearfix">
                <li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                <li><a href="#"><span class="fa fa-pinterest"></span></a></li>            
            </ul>
        </div>
    </div><!-- / Hidden Bar Wrapper -->
</section>
<!-- End / Hidden Bar -->
<!--Main Header End-->