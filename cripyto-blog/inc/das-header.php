<!--Dash Header Start-->
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<title>Mycryptovision</title>
<!-- Stylesheets -->
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/plugins/revolution/css/settings.css" rel="stylesheet" type="text/css"><!-- REVOLUTION SETTINGS STYLES -->
<link href="assets/plugins/revolution/css/layers.css" rel="stylesheet" type="text/css"><!-- REVOLUTION LAYERS STYLES -->
<link href="assets/plugins/revolution/css/navigation.css" rel="stylesheet" type="text/css"><!-- REVOLUTION NAVIGATION STYLES -->
<link href="assets/css/style.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<!--Color Switcher Mockup-->
<link href="assets/css/color-switcher-design.css" rel="stylesheet">

<!--Color Themes-->
<link id="theme-color-file" href="assets/css/color-themes/default-theme.css" rel="stylesheet">

<!--Favicon-->
<link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">
<link rel="icon" href="assets/images/favicon.png" type="image/x-icon">
<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
</head>

<body>

<div class="page-wrapper"> 	
    <!-- Preloader -->
    <div class="preloader"></div> 	
    <!-- Main Header -->
    <header class="main-header">
    	<!--Header-Upper-->
        <div class="header-upper">
        	<div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="logo-outer">
                            <div class="logo"><a href="index-2.html"><span class="letter">n</span><img src="assets/images/logo.png" alt="" /></a></div>
                        </div>
                    </div>
                </div>
			</div>
        </div>
        <!--End Header Upper-->
        <!--Header Lower-->
        <div class="header-lower">
        	<div class="auto-container">
                    <div class="nav-outer clearfix">
                    <!-- Main Menu -->
                    <nav class="main-menu">
                        <div class="navbar-header">
                            <!-- Toggle Button -->    	
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        </div>                        
                        <div class="navbar-collapse collapse clearfix" id="bs-example-navbar-collapse-1">
                            <ul class="navigation clearfix">
                            <li><a href="index.html">Home</a></li>
                                <li class="dropdown"><a href="">name<i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <li><a href="profile.html">Profile</a></li>
                                    <li><a href="#">Logout</a></li>
                                </ul>
                                
                                </li>
                            </ul>
                        </div>
                    </nav>                    
                    <!-- Hidden Nav Toggler -->
                     <div class="nav-toggler">
                         <button class="hidden-bar-opener"><span class="icon qb-menu1"></span></button>
                     </div>                    
                </div>
            </div>
        </div>
        <!--End Header Lower-->    
    </header>
    <!--End Header Style Two -->    
    <!-- Hidden Navigation Bar -->
    <section class="hidden-bar left-align">        
        <div class="hidden-bar-closer">
            <button><span class="qb-close-button"></span></button>
        </div>        
        <!-- Hidden Bar Wrapper -->
        <div class="hidden-bar-wrapper">
            <div class="logo">
            	<a href="index.php"><span class="letter">n</span><img src="assets/images/mobile-logo.png" alt="" /></a>
            </div>
            <!-- .Side-menu -->
            <div class="side-menu">
            	<!--navigation-->
                <ul class="navigation clearfix">
                    <li class="current"><a href="profile.html">Profile</a></li>
                    <li><a href="dashboard.html">Dashboard</a></li>
                    <li><a href="#">Logout</a></li>
                </ul>
            </div>
            <!-- /.Side-menu -->          
        </div><!-- / Hidden Bar Wrapper -->
    </section>
    <!-- End / Hidden Bar -->
<!--Dash Header End-->