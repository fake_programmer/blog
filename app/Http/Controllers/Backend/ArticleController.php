<?php

namespace App\Http\Controllers\Backend;


use App\User;

use App\Models\Articles;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    public function articleList()
    {
        $allArticle=Articles::with('articleUser')->with('articleCate')->paginate(10);
        //dd($article);
        return view('Backend.article.articleList', compact('allArticle'));
    }

    public function activeArticle($id)
    {
        //$status = "active";
        $updateStatus = Articles::where('id', $id)->Update([
            'status' => "active",
        ]);
        toastr()->add('success', 'Article Active Successfully.', 'Success');
        return redirect()->back();

    }

    Public function InactiveArticle($id)
    {
        //$status = "inactive";
        $updateStatus = Articles::where('id', $id)->Update([
            'status' => "inactive",
        ]);
        toastr()->add('error', 'Article Inactive Successfully.', 'Success');
        return redirect()->back();
    }
}
