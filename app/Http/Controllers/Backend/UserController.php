<?php

namespace App\Http\Controllers\Backend;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class UserController extends Controller
{
    public function adminIndex()
    {
        return view('Backend.index');
    }

    public function authorList()
    {
        $authorList = User::all();

        return view('Backend.author.author', compact('authorList'));
    }

    public function activeAuthor($id)
    {
        $status = "active";
        $updateStatus = User::where('id', $id)->Update([
            'status' => $status,
        ]);
        session()->flash('message', 'User Active successfully.');
        return redirect()->back();
    }

    public function InactiveAuthor($id)
    {
        $status = "Inactive";
        $updateStatus = User::where('id', $id)->Update([
            'status' => $status,
        ]);
        session()->flash('message', 'User Inactive successfully.');
        return redirect()->back();
    }


}
