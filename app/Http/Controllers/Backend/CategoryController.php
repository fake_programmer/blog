<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Catagories;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function category()
    {
        $allCate=Catagories::orderBy('id')->paginate(10);
        //dd($allCate);
        return view('Backend.category.categoryList', compact('allCate'));
    }
    public function createCategory()
    {
        return view('Backend.category.createCategory');
    }
    public function createCategoryProcess(Request $request)
    {
        if (!empty($request->all())) {
            //dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $checkCategory = Catagories::select('name')->where('name', $request->name)->first();
            //dd($checkCategory);
            if (!empty($checkCategory)) {
                toastr()->add('error', 'Sorry !! This category allready added.', 'Ohh');
                return redirect()->route('category');
            } else {
                Catagories::create([
                    'name'   => $request->input('name'),
                ]);
                toastr()->add('success', 'Category added successfully.', 'Success');
                return redirect()->back();
            }
        }
    }
    public function editCategory($id)
    {
        $category=Catagories::where('id', $id)->first();
        //dd($category);
        return view('Backend.category.editCategory', compact('category'));
    }
    public function editCategoryProcess(Request $request, $id)
    {
        if (!empty($request->all())) {
            //dd($request->all());
            $validator = Validator::make($request->all(), [
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            Catagories::where('id', $id)->update([
                'name'   => $request->input('name'),
            ]);
            toastr()->add('success', 'Category Updated successfully.', 'Success');
            return redirect()->route('category');
        }
    }
    public function deleteCategory($id)
    {
        $category = Catagories::find($id);
        $category->delete();
        session()->flash('message', 'Category deleted successfully.');
        return redirect()->route('category');
    }
}


