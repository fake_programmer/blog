<?php

namespace App\Http\Controllers\Frontend;
use App\Models\Catagories;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
//        $userData = Userdata::latest()->paginate(5);
//        return view('Frontend.index', compact('userData'))
//            ->with('i', (request()->input('page', 1) - 1) * 5);

        return view('Frontend.index');
    }

    public function registration()
    {
        return view('Frontend.author.registration');
    }

    Public function registrationProcess(Request $request)
    {
        if (!empty($request->all())) {
            $validator = Validator::make($request->all(), [
                'name' => 'required|regex:/^[a-zA-Z0-9\'\s]+$/',
                'nic_name' => 'required',
                'email' => 'required',
                'address' => 'required',
                'bio' => 'required',
                'password'  => 'required|min:6',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $checkUser = User::select('email')->where('email', $request->email)->first();
            //dd($checkUser);

            if (!empty($checkUser)) {
                session()->flash('message', 'Sorry !! This user is already added.');
                return redirect()->route('registration');
            } else {
                User::create([
                    'name' => $request->input('name'),
                    'nic_name' => $request->input('nic_name'),
                    'email' => $request->input('email'),
                    'address' => $request->input('address'),
                    'bio' => $request->input('bio'),
                    'password' => bcrypt($request->input('password')),

                ]);
                session()->flash('message', 'User Registered Successfully.');
                return redirect()->back();
            }
        }
    }
}
