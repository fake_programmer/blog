<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Articles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $articleData=Articles::with('articleUser')->with('articleCate')->where('status', "active")->get();
        //dd($articleData);
        return view('Frontend.index', compact('articleData'));
    }
}
