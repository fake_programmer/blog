<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Articles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Catagories;

class AuthorController extends Controller
{
    public  function authorDashboard()
    {
        $article=Articles::all();
        $category=Catagories::all();
        return view('Frontend.author.authorDashboard', compact('article', 'category' ));
    }
    public  function addPost()
    {
        $cateName=Catagories::all();
        return view('Frontend.author.addPost', compact('cateName'));
    }
    public  function addPostProcess(Request $request)
    {
        if (!empty($request->all())) {
            //dd($request->all());
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
                //'img' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $checkArticle = Articles::select('title')->where('title', $request->title)->first();
            //dd($checkArticle);
            if (!empty($checkArticle)) {
                session()->flash('message', 'Sorry !! This Article Allready Added.');
                return redirect()->route('viewPost');
            } else {
                // Handle File Upload
                if($request->hasFile('img')){
                    // Get filename with the extension
                    $filenameWithExt = $request->file('img')->getClientOriginalName();
                    // Get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // Get just ext
                    $extension = $request->file('img')->getClientOriginalExtension();
                    // Filename to store
                    $fileNameToStore= $filename.'_'.time().'.'.$extension;
                    // Upload Image
                    $path = $request->file('img')->storeAs('img', $fileNameToStore);
                } else {
                    $fileNameToStore = 'noimage.jpg';
                }
                Articles::create([
                    'title'   => $request->input('title'),
                    'cat_id'   => $request->input('cat_id'),
                    'user_id'   => 1,//auth user id
                    'description'   => $request->input('description'),
                    'img'   => $fileNameToStore,
                    'vedio'   => $request->input('vedio')
                ]);
                toastr()->add('success', 'Article added successfully.', 'Success');
                return redirect()->route('addPost');
            }
        }
    }
    public  function viewPost()
    {
        $allPost=Articles::with('articleUser')->paginate(10);
        //dd($allPost);
        return view('Frontend.author.viewPost', compact('allPost'));
    }
    public  function editPost($id)
    {
        $articleData=Articles::with('articleCate')->where('id', $id)->first();
        //dd($articleData);
        $cateName=Catagories::all();
        return view('Frontend.author.editPost', compact('cateName', 'articleData'));
    }
    public  function editPostProcess(Request $request, $id)
    {
        if (!empty($request->all())) {
            //dd($request->all());
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'description' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            // Handle File Upload
            if($request->hasFile('img')){
                // Get filename with the extension
                $filenameWithExt = $request->file('img')->getClientOriginalName();
                // Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                // Get just ext
                $extension = $request->file('img')->getClientOriginalExtension();
                // Filename to store
                $fileNameToStore= $filename.'_'.time().'.'.$extension;
                // Upload Image
                $path = $request->file('img')->storeAs('public/assets/images/gallery', $fileNameToStore);
            } else {
                $fileNameToStore = 'noimage.jpg';
            }
            Articles::where('id', $id)->update([
                'title'   => $request->input('title'),
                'cat_id'   => $request->input('cat_id'),
                'user_id'   => 1,//auth user id
                'description'   => $request->input('description'),
                'img'   => $fileNameToStore,
                'vedio'   => $request->input('vedio')
            ]);
            toastr()->add('success', 'Article Updated successfully.', 'Success');
            return redirect()->route('viewPost');
        }
    }
}
