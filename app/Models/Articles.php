<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    protected $fillable = ['title','cat_id','user_id','description','img','vedio'];
    public function articleUser()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function articleCate()
    {
        return $this->hasOne('App\Models\Catagories', 'id', 'user_id');
    }
}
