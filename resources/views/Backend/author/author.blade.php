@extends('Backend.master')
@section('content')
    <!-- Alert- message -->
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <!-- Page content -->
    <div id="page-content">
        <!-- Dashboard Header -->

        <div class="content-header content-header-media">
            <div class="header-section">
                <div class="row">
                    <!-- Main Title (hidden on small devices for the statistics to fit) -->
                    <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                        <h1>Author List</h1>
                    </div>
                    <!-- END Main Title -->
                </div>
            </div>

            <img src="img/placeholders/headers/dashboard_header.jpg" alt="header image" class="animation-pulseSlow">
        </div>
        <!-- END Dashboard Header -->

        <!-- Mini Top Stats Row -->
        <div class="row">
            <div class="box round first grid">
                <h2>Author List</h2>
                <div class="block">

                    <table class="data display datatable" id="example">
                        <thead>
                        <tr>
                            <th width="10%">No</th>
                            <th width="25%">Name</th>
                            <th width="20%">User Name</th>
                            <th width="25%">Email</th>
                            <th width="20%">Action</th>

                        </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1;?>
                        @foreach($authorList as $data)
                            <tr class="odd gradeX">
                                <td>{{$i++}}</td>
                                <td><p>{{$data->name}}</p></td>
                                <td><p>{{$data->nic_name}}</p></td>
                                <td><p>{{$data->email}}</p></td>
                                <td><a class="btn btn-info" href="{{route('activeAuthor', $data->id)}}">Active</a>
                                    <a class="btn btn-danger" onclick="return confirm('Are you Sure to Delete!');"
                                       href="{{route('InactiveAuthor', $data->id)}}">Inactive</a></td>
                            </tr>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Mini Top Stats Row -->

    </div>
    <!-- END Page Content -->
@stop