@extends('Backend.master')
@section('content')
<!-- Mini Top Stats Row -->
@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif
<div class="row">
    <div class="box round first grid">
        <h2>Category List</h2>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                <tr>
                    <th width="33%">No</th>
                    <th width="33%">Category Name</th>
                    <th width="33%">Action</th>

                </tr>
                </thead>
                <tbody>
                <?php $i=1;?>
                @foreach($allCate as $data)
                <tr class="odd gradeX">
                    <td>{{$i++}}</td>
                    <td><p>{{$data->name}}</p></td>
                    <td><a class="btn btn-info" href="{{route('editCategory', $data->id)}}">Edit</a>
                        <a class="btn btn-danger" onclick="return confirm('Are you Sure to Delete!');" href="{{route('deleteCategory', $data->id)}}">Delete</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        {{ $allCate->links() }}
    </div>
</div>
<!-- END Mini Top Stats Row -->
@stop
