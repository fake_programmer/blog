@extends('Backend.master')
@section('content')
    <!-- Mini Top Stats Row -->
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <div class="row">
        <form action="{{route('editCategoryProcess', $category->id)}}" method="post" role="form" class="form-horizontal form-bordered ui-formwizard">
        @method('put')
        @CSRF
        <!-- END Step Info -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="category">Category Name</label>
                <div class="col-md-5">
                    <input id="category" name="name" class="form-control " value="{{$category->name}}" type="text">
                </div>
            </div>
            <!-- END First Step -->
            <!-- Form Buttons -->
            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-sm btn-primary" id="next4" value="Next">Submit</button>
                </div>
            </div>
            <!-- END Form Buttons -->
        </form>
    </div>
    <!-- END Mini Top Stats Row -->
@stop
