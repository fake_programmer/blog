<!-- Admin Dashboard Header Start-->
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Mycryptovision Admin Dashboard</title>
    <meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="assets/admin/img/favicon.png">
    <!-- END Icons -->
    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap.min.css')}}">
    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/plugins.css')}}">
    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/themes.css')}}">
    <!-- END Stylesheets -->
    <!-- Modernizr (browser feature detection library) -->
    <script src="{{asset('assets/admin/js/vendor/modernizr.min.js')}}"></script>
</head>
<body>
<div id="page-wrapper">
    <!-- Preloader -->
    <div class="preloader themed-background">
        <h1 class="push-top-bottom text-light text-center"><strong>My</strong>cryptovision</h1>
        <div class="inner">
            <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
            <div class="preloader-spinner hidden-lt-ie10"></div>
        </div>
    </div>
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <!-- Main Sidebar Start-->
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <a href="index.html" class="sidebar-brand">
                        <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Deshi</strong>Travels</span>
                    </a>
                    <!-- User Info -->
                    <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                        <div class="sidebar-user-avatar">
                            <a href="index.html">
                                <img src="{{asset('assets/admin/img/placeholders/avatars/avatar2.jpg')}}" alt="avatar">
                            </a>
                        </div>
                        <div class="sidebar-user-name">Admin User Name</div>
                        <div class="sidebar-user-links">
                            <a href="profile.html" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                            <a href="inbox.html" data-toggle="tooltip" data-placement="bottom" title="Messages"><i class="gi gi-envelope"></i></a>
                            <a href="?action=logout" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                        </div>
                    </div>
                    <!-- END User Info -->
                    <!-- Sidebar Navigation -->
                    <ul class="sidebar-nav">
                        <li>
                            <a href="index.html"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                        </li>

                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Category</span></a>
                            <ul>
                                <li>
                                    <a href="{{route('createCategory')}}">Add Category</a>
                                </li>
                                <li>
                                    <a href="{{route('category')}}">Category List</a>
                                </li>

                            </ul>
                        </li>
                        <li>
                            <a href="{{route('authorList')}}"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Authors</span></a>
                        </li>
                        <li>
                            <a href="{{route('articleList')}}"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Article</span></a>
                        </li>
                        <li>
                            <a href="comment.html"><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Comment</span></a>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-stopwatch sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Website Settings</span></a>
                            <ul>
                                <li>
                                    <a href="facebooklink.html">Add Facebook Links</a>
                                </li>
                                <li>
                                    <a href="twitter.html">Add twitter Links</a>
                                </li>
                                <li>
                                    <a href="google-plus.html">Add Google-plus Links</a>
                                </li>
                                <li>
                                    <a href="aboutus.html">About-Us Page</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- END Sidebar Navigation -->
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- Main Sidebar End-->

        <!-- Main Container -->
        <div id="main-container">

            <header class="navbar navbar-default">
                <!-- Search Form -->
                <form action="page_ready_search_results.html" method="post" class="navbar-form-custom">
                    <div class="form-group">
                        <input type="text" id="top-search" name="top-search" class="form-control" placeholder="Search..">
                    </div>
                </form>
                <!-- END Search Form -->

                <!-- Right Header Navigation -->
                <ul class="nav navbar-nav-custom pull-right">

                    <!-- User Dropdown -->
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{asset('assets/admin/img/placeholders/avatars/avatar2.jpg')}}" alt="avatar"> <i class="fa fa-angle-down"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                            <li class="dropdown-header text-center">Account</li>
                            <li>

                                <a href="inbox.html">
                                    <i class="fa fa-envelope-o fa-fw pull-right"></i>
                                    Messages
                                </a>

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="profile.html">
                                    <i class="fa fa-user fa-fw pull-right"></i>
                                    Profile
                                </a>

                            </li>
                            <li class="divider"></li>
                            <li>

                                <a href="?action=logout"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                            </li>

                        </ul>
                    </li>
                    <!-- END User Dropdown -->
                </ul>
                <!-- END Right Header Navigation -->
            </header>
            <!-- END Header -->
            <!-- Admin Dashboard Header Start-->


            <!-- Page content -->
            <div id="page-content">
                <!-- Dashboard Header -->

                <div class="content-header content-header-media">
                    <div class="header-section">
                        <div class="row">
                            <!-- Main Title (hidden on small devices for the statistics to fit) -->
                            <div class="col-md-4 col-lg-6 hidden-xs hidden-sm">
                                <h1>Welcome <strong>User Name</strong></h1>
                            </div>
                            <!-- END Main Title -->
                        </div>
                    </div>

                    <img src="{{asset('assets/admin/img/placeholders/headers/dashboard_header.jpg')}}" alt="header image" class="animation-pulseSlow">
                </div>
                <!-- END Dashboard Header -->
