</div>
<!-- END Page Content -->

<!-- Footer -->
<footer class="clearfix">
    <div class="pull-right">
        Mycryptovision &copy; 2018. <span class="dev">Developed By QSS</span>
    </div>

</footer>
<!-- END Footer -->
</div>
<!-- END Main Container -->
</div>
<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>



<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="{{asset('assets/admin/js/vendor/jquery.min.js')}}"></script>
<script src="{{asset('assets/admin/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/js/plugins.js')}}"></script>
<script src="{{asset('assets/admin/js/app.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key="></script>
<script src="{{asset('assets/admin/js/helpers/gmaps.min.js')}}"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="{{asset('assets/admin/js/pages/index.js')}}"></script>
<script src="{{asset('assets/admin/js/helpers/ckeditor/ckeditor.js')}}"></script>
<script>$(function(){ Index.init(); });</script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
{!! Toastr::message() !!}
</body>
</html>
