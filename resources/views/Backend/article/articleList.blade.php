@extends('Backend.master')
@section('content')
<!-- Mini Top Stats Row -->
<div class="row">
    <div class="box round first grid">
        <h2>Article List</h2>
        <div class="block">
            <table class="data display datatable" id="example">
                <thead>
                    <tr>
                        <th width="10%">No</th>
                        <th width="20%">Title</th>
                        <th width="10%">Author Name</th>
                        <th width="15%">Image</th>
                        <th width="15%">Category Name</th>
                        <th width="10%">Status</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>
                    <tbody>
                    <?php $i=1;?>
                    @foreach($allArticle as $data)
                    <tr class="odd gradeX">
                        <td>{{$i++}}</td>
                        <td><p>{{$data->title}}</p></td>
                        <td><p>{{$data->articleUser->name}}</p></td>
                        <td><img src="{{asset('img')}}/{{$data->img}}" style='height: 50px; width: 80px; margin-bottom: 10px; ' alt=""></td>
                        <td><p>{{$data->articleCate->name}}</p></td>
                        <td>
                            <p>{{$data->status}}</p>
                        </td>
                        <td>
                            @if($data->status=="inactive")
                            <a class="btn btn-info" onclick="return confirm('Are you Sure to Active!');" href="{{route('activeArticle', $data->id)}}">Active</a>
                            @else
                            <a class="btn btn-danger" onclick="return confirm('Are you Sure to InActive!');" href="{{route('InactiveArticle', $data->id)}}">Inactive</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{ $allArticle->links() }}
    </div>
</div>
    <!-- END Mini Top Stats Row -->
@stop
