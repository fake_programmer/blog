@extends('Frontend.master')
@section('content')
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar dashboard">

                        <!--Category Widget-->
                        <div class="sidebar-widget categories-widget">
                            <div class="sidebar-title">
                                <h2>Menu</h2>
                            </div>
                            <ul class="cat-list dash_menu">
                                <li class="clearfix"><a href="{{route('authorDashboard')}}">Dashboard</a></li>
                                <li class="clearfix"><a href="{{route('addPost')}}">Add Post</a></li>
                                <li class="clearfix"><a href="{{route('viewPost')}}">View Post</a></li>
                            </ul>
                        </div>
                        <!--End Category Widget-->

                    </aside>
                </div>
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="content">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <h2>View Posts</h2>
                        </div>

                        <div class="row clearfix">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Author Name</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i=1;?>
                                @foreach($allPost as $data)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$data->title}}</td>
                                    <td>{{$data->articleUser->name}}</td>
                                    <td>{{$data->created_at->format('d/m/Y')}}</td>
                                    <td>
                                        {{--<a href="{{route('deletePost', $data->id)}}" class="btn btn-danger" onclick="return confirm('Are you Sure to Delete!');">Delete</a>--}}
                                        <a href="{{route('editPost', $data->id)}}" class=" btn btn-info">Edit</a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $allPost->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->
@stop
