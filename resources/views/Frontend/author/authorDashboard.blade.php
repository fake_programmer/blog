@extends('Frontend.master')
@section('content')
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar dashboard">
                        <!--Category Widget-->
                        <div class="sidebar-widget categories-widget">
                            <div class="sidebar-title">
                                <h2>Menu</h2>
                            </div>
                            <ul class="cat-list dash_menu">
                                <li class="clearfix"><a href="{{route('authorDashboard')}}">Dashboard</a></li>
                                <li class="clearfix"><a href="{{route('addPost')}}">Add Post</a></li>
                                <li class="clearfix"><a href="{{route('viewPost')}}">View Post</a></li>
                            </ul>
                        </div>
                        <!--End Category Widget-->
                    </aside>
                </div>
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="content">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <h2>Dashboard</h2>
                        </div>
                        <div class="row clearfix">
                            <!--Block One-->
                            <div class="dashboard-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="dash-icon">
                                        <i class="fa fa-clipboard"></i>
                                    </div>
                                    <div class="lower-box">
                                        <h4>Total <span>{{count($article)}}</span> Posts</h4>
                                    </div>
                                </div>
                            </div>
                            <!--End Block-->
                            <!--Block One-->
                            <div class="dashboard-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="dash-icon">
                                        <i class="fa fa-clipboard"></i>
                                    </div>
                                    <div class="lower-box">
                                        <h4>Total <span>{{count($category)}}</span> Category</h4>
                                    </div>
                                </div>
                            </div>
                            <!--End Block-->
                            <!--Block One-->
                            <div class="dashboard-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="dash-icon">
                                        <i class="fa fa-edit"></i>
                                    </div>
                                    <div class="lower-box">
                                        <h4><a href="{{route('addPost')}}">Add New Post</a></h4>
                                    </div>
                                </div>
                            </div>
                            <!--End Block-->
                            <!--Block One-->
                            <div class="dashboard-block col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="dash-icon">
                                        <i class="fa fa-eye"></i>
                                    </div>
                                    <div class="lower-box">
                                        <h4><a href="{{route('viewPost')}}">View All Posts</a></h4>
                                    </div>
                                </div>
                            </div>
                            <!--End Block-->
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!--End Sidebar Page Container-->
@stop
