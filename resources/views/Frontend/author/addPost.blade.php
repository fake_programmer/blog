@extends('Frontend.master')
@section('content')
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar dashboard">

                        <!--Category Widget-->
                        <div class="sidebar-widget categories-widget">
                            <div class="sidebar-title">
                                <h2>Menu</h2>
                            </div>
                            <ul class="cat-list dash_menu">
                                <li class="clearfix"><a href="{{route('authorDashboard')}}">Dashboard</a></li>
                                <li class="clearfix"><a href="{{route('addPost')}}">Add Post</a></li>
                                <li class="clearfix"><a href="{{route('viewPost')}}">View Post</a></li>
                            </ul>
                        </div>
                        <!--End Category Widget-->

                    </aside>
                </div>
                <!-- Success or Error Message Start -->
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
            @endif
            <!-- Success or Error Message End -->
                <!--Content Side-->
                <div class="content-side col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="content">
                        <!--Sec Title-->
                        <div class="sec-title">
                            <h2>Add New Post</h2>
                        </div>

                        <div class="row clearfix">
                            <div class="post-form">
                                <form class="form-horizontal" action="{{route('addPostProcess')}}" method="post"
                                      enctype="multipart/form-data">
                                    @CSRF
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="title">Post Title:</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="title" name="title"
                                                   placeholder="Enter Post Title">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="cat_id">Category Name:</label>
                                        <div class="col-sm-7">
                                            <select class="form-control" name="cat_id" id="cat_id">
                                                <option value="">Select a Category</option>
                                                @foreach($cateName as $data)
                                                    <option value="{{$data->id}}">{{$data->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="description">Post
                                            Description:</label>
                                        <div class="col-sm-7">
                                            <textarea class="ckeditor form-control" name="description" id="description"
                                                      cols="30" rows="10"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="img">Post Image:</label>
                                        <div class="col-sm-7">
                                            <input type="file" id="img" class="form-control" name="img">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3" for="vedio">Post Vedio Link:</label>
                                        <div class="col-sm-7">
                                            <textarea class="ckeditor form-control" name="vedio" id="vedio" cols="30"
                                                      rows="10"></textarea>
                                        </div>
                                    </div>
                                    {{--                                    <div class="form-group">--}}
                                    {{--                                    <label class="control-label col-sm-3" for="pub_date">Publication Date:</label>--}}
                                    {{--                                    <div class="col-sm-7">--}}
                                    {{--                                    <input type="date" id="pub_date" class="form-control" name="pub_date">--}}
                                    {{--                                    </div>--}}
                                    {{--                                    </div>--}}
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-7">
                                            <button type="submit" class="btn btn-default">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->
@stop
