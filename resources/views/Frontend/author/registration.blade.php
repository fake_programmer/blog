@extends('Frontend.master')
@section('content')

    <div class="sidebar-page-container">
        <div class="auto-container">
            <!-- Success or Error Message Start -->
                @if (session()->has('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
            <!-- Success or Error Message End -->
            <div class="row clearfix">
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                    <div class="content">
                        <div class="row clearfix">
                            <div class="register-form">

                                <form class="form-horizontal" action="{{ route('registrationProcess') }}" method="post" role="form" >
                                    @csrf
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="name">Name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="name" name="name"
                                                   placeholder="Enter Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="nic_name">User Name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="nic_name" name="nic_name"
                                                   placeholder="Enter User Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">Email:</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" name="email"
                                                   placeholder="Enter email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="bio">Bio:</label>
                                        <div class="col-sm-10">
                                            <textarea name="bio" id="bio" class="form-control" cols="30"  rows="10"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="address">Address</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="address" name="address"
                                                   placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="password">Password:</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="pwd"> Re-type  Password:</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="pwd" name="" placeholder="Enter password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-default">Register</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--End Sidebar Page Container-->
@stop
