@extends('Frontend.master')
@section('content')
<!--Sidebar Page Container-->
<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Content Side-->
            <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="content">
                    <!--Sec Title-->
                    <div class="sec-title">
                        <h2>Article</h2>
                    </div>

                    <div class="row clearfix">
                        <!-- Styled Pagination Start -->
                        <div class="styled-pagination">
                            <ul class="clearfix">
                                <!--pagination -->
                                <li><a href="#" class="active">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a class="next" href="#"><span class="fa fa-angle-right"></span></a></li>
                                <!--pagination -->
                            </ul>
                        </div>
                        <!-- Styled Pagination End -->
                        <!--News Block One-->
                        @foreach($articleData as $data)
                        <div class="news-block-one col-md-4 col-sm-6 col-xs-12">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="blog-single.html"><img src="{{asset('img')}}/{{$data->img}}" alt="" /></a>
                                </div>
                                <div class="lower-box">
                                    <h3><a href="singlepage.html">{{$data->title}}</a></h3>
                                    <div class="post-date">{{$data->created_at->format('d-m-Y')}} / {{$data->articleUser->name}}</div>
                                    <div class="text">{{\Illuminate\Support\Str::words($data->description, 50, '...')}}</div>
                                    <a href="#" class="read-more">Read More <span class="arrow ion-ios-arrow-thin-right"></span></a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <!--End News Block-->
                    </div>

                </div>

                <!-- Styled Pagination Start -->
                <div class="styled-pagination">
                    <ul class="clearfix">
                        <!--pagination -->
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a class="next" href="#"><span class="fa fa-angle-right"></span></a></li>
                        <!--pagination -->
                    </ul>
                </div>
                <!-- Styled Pagination End -->
            </div>
            <!--Right Sidebar Start-->
            <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <aside class="sidebar default-sidebar right-sidebar">

                    <!--Social Widget-->
                    <div class="sidebar-widget sidebar-social-widget">
                        <div class="sidebar-title">
                            <h2>Follow Us</h2>
                        </div>
                        <ul class="social-icon-one alternate">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li class="twitter"><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li class="g_plus"><a href="%"><span class="fa fa-google-plus"></span></a></li>
                        </ul>
                    </div>
                    <!--End Social Widget-->

                    <!--Category Widget-->
                    <div class="sidebar-widget categories-widget">
                        <div class="sidebar-title">
                            <h2>Categories</h2>
                        </div>
                        <ul class="cat-list">
                            <li class="clearfix"><a href="category.html">PHP</a></li>
                            <li class="clearfix"><a href="category.html">JS</a></li>
                            <li class="clearfix"><a href="category.html">HTML</a></li>
                        </ul>
                    </div>
                    <!--End Category Widget-->

                    <!--Category Widget-->
                    <div class="sidebar-widget categories-widget">
                        <div class="sidebar-title">
                            <h2>Recent Post</h2>
                        </div>
                        <article class="widget-post">
                            <figure class="post-thumb"><a href="singlepage.html"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="assets/images/resource/news-1.jpg" alt=""></a><div class="overlay"><span class="icon qb-play-arrow"></span></div></figure>
                            <div class="text"><a href="singlepage.html">Article Title</a></div>
                            <div class="post-info">22-06-2019</div>
                        </article>
                        <article class="widget-post">
                            <figure class="post-thumb"><a href="singlepage.html"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="assets/images/resource/news-5.jpg" alt=""></a><div class="overlay"><span class="icon qb-play-arrow"></span></div></figure>
                            <div class="text"><a href="singlepage.html">Article Title</a></div>
                            <div class="post-info">22-06-2019</div>
                        </article>
                        <article class="widget-post">
                            <figure class="post-thumb"><a href="singlepage.html"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="assets/images/resource/news-6.jpg" alt=""></a><div class="overlay"><span class="icon qb-play-arrow"></span></div></figure>
                            <div class="text"><a href="singlepage.html">Article Title</a></div>
                            <div class="post-info">22-06-2019</div>
                        </article>
                        <article class="widget-post">
                            <figure class="post-thumb"><a href="singlepage.html"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="assets/images/resource/news-7.jpg" alt=""></a><div class="overlay"><span class="icon qb-play-arrow"></span></div></figure>
                            <div class="text"><a href="singlepage.html">Article Title</a></div>
                            <div class="post-info">22-06-2019</div>
                        </article>
                        <article class="widget-post">
                            <figure class="post-thumb"><a href="singlepage.html"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="assets/images/resource/news-8.jpg" alt=""></a><div class="overlay"><span class="icon qb-play-arrow"></span></div></figure>
                            <div class="text"><a href="singlepage.html">Article Title</a></div>
                            <div class="post-info">22-06-2019</div>
                        </article>
                        <article class="widget-post">
                            <figure class="post-thumb"><a href="singlepage.html"><img class="wow fadeIn" data-wow-delay="0ms" data-wow-duration="2500ms" src="assets/images/resource/news-9.jpg" alt=""></a><div class="overlay"><span class="icon qb-play-arrow"></span></div></figure>
                            <div class="text"><a href="singlepage.html">Article Title</a></div>
                            <div class="post-info">22-06-2019</div>
                        </article>
                    </div>

                </aside>
            </div>
            <!--Right Sidebar End-->
        </div>
    </div>
</div>
<!--End Sidebar Page Container-->
@stop
