<!--Main Header Start-->
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Mycryptovision</title>
    <!-- Stylesheets -->
    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    <link href="{{asset('assets/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/revolution/css/settings.css')}}" rel="stylesheet" type="text/css">
    <!-- REVOLUTION SETTINGS STYLES -->
    <link href="{{asset('assets/plugins/revolution/css/layers.css')}}" rel="stylesheet" type="text/css">
    <!-- REVOLUTION LAYERS STYLES -->
    <link href="{{asset('assets/plugins/revolution/css/navigation.css')}}" rel="stylesheet" type="text/css">
    <!-- REVOLUTION NAVIGATION STYLES -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet">
    <!--Color Switcher Mockup-->
    <link href="{{asset('assets/css/color-switcher-design.css')}}" rel="stylesheet">
    <!--Color Themes-->
    <link id="theme-color-file" href="{{asset('assets/css/color-themes/default-theme.css')}}" rel="stylesheet">
    <!--Favicon-->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('assets/images/favicon.png')}}" type="image/x-icon">
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <script src="//platform-api.sharethis.com/js/sharethis.js#property=5b9f819ba51ed30011fe7083&product=inline-share-buttons"></script>
</head>
<body>
<div class="page-wrapper">
    <!-- Preloader -->
    <!-- <div class="preloader"></div> -->
    <!-- Main Header -->
    <header class="main-header">
        <!--Header-Upper-->
        <div class="header-upper">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="logo-outer">
                            <div class="logo"><a href="{{route('index')}}"><img src="assets/images/logo.png" alt=""/></a></div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="header-search">
                            <form method="get" action="#">
                                <div class="form-group">
                                    <input type="search" name="search" value=""
                                           placeholder="Find more articles, type your search..." required>
                                    <button type="submit" class="search-btn">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Header Upper-->

